export const FooterFrame = () => (
  <svg
    width="1440"
    height="408"
    viewBox="0 0 1440 408"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 47C0 47 167.5 102.5 395.5 93.5C623.5 84.5 885.5 0 1102.5 0C1319.5 0 1440 47 1440 47V408H0V47Z"
      fill="black"
      fill-opacity="0.8"
    />
  </svg>
);
